import React from 'react';



class Form extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        Name: '',
        EmailAddress:'Aya.Saafan@webgenstudio.com',
        EmailSubject:'',
        EmailBody:'',
      };
    }
    myChangeHandler = (event) => {
      let nam = event.target.name;
      let val = event.target.value;
      this.setState({[nam]: val});
    }

    InvokeHumanApproval(e){      
      var params ='EmailSubject='+this.state.EmailSubject+'&EmailAddress='+this.state.EmailAddress+'&EmailBody='+this.state.EmailBody+ '&Name='+this.state.Name
      var requestOptions = {
        method: 'POST',
        redirect: 'follow',
        mode: 'no-cors' 
      };
      
      fetch("https://zqzp5evboa.execute-api.us-east-2.amazonaws.com/HumanApproval/invoke?"+params, requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));

      alert('Waiting for the client to approve/reject.');      
    }
    
    render() {
      return (
        <div>
          <div class="jumbotron">
            <h1>Human Approval</h1>
            <p>Send an Approve/Reject Email using AWS API Gateway</p>
            <h6>invoke?EmailAddress={this.state.EmailAddress}&Name={this.state.Name}&EmailSubject={this.state.EmailSubject}&EmailBody={this.state.EmailBody}</h6>

            </div>
        
   
        <form>         
          <div className="form-group">
            <label>Name:</label>
            <input
              className="form-control" 
              type='text'
              name='Name'
              onChange={this.myChangeHandler}/>
          </div>
          <div className="form-group">
            <label>Email Address:</label>
            <input 
              className="form-control" 
              type='text'
              name='EmailAddress'
              onChange={this.myChangeHandler}/>
          </div>
          <div className="form-group">
            <label>Email Subject:</label>
            <input
              className="form-control" 
              type='text'
              name='EmailSubject'
              onChange={this.myChangeHandler}/>
          </div>
          <div className="form-group">
            <label>Email Body:</label>
            <textarea
              className="form-control" 
              type='text'
              name='EmailBody'
              onChange={this.myChangeHandler}/>
          </div>

        <br/> 
        <button className="btn btn-primary" onClick={(e) => {this.InvokeHumanApproval(e)}}>Invoke</button>
        </form>
        </div>
      );
    }
  }

export default Form;